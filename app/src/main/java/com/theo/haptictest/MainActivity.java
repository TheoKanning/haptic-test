package com.theo.haptictest;

import android.os.Vibrator;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;


public class MainActivity extends ActionBarActivity {

    ImageButton ibHaptic;
    int xy[] = new int[2];
    int center_x = 0;
    int center_y = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ibHaptic = (ImageButton) findViewById(R.id.ibMain_Haptic);

        ibHaptic.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {

                    center_x = ibHaptic.getWidth() / 2;
                    center_y = ibHaptic.getHeight() / 2;

                    float x = event.getX();
                    float y = event.getY();

                    //center_x is also the radius of the circle
                    if ((x - center_x) * (x - center_x) + (y - center_y) * (y - center_y) <= center_x * center_x) {
                        //Send haptic
                        Vibrator vibe = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                        vibe.vibrate(250);
                    }

                    Log.d("Main", "Button touched X:" + event.getX() + " Y:" + event.getY());
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
